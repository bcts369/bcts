---
title: "About"
date: 2018-11-30T14:31:00+08:00
anchor: "about"
weight: 6
draft: false
---

ブロックチェーンを含めた仮想通貨関連のソフトウエア開発に2015年から参加しています。

好きな通貨は、Stellar、Ripple、Nem

フルスタックエンジニアですが、サーバーサイドが好きです。

本ツールをご購入前のご質問などはTwitterで承っておりますので、お気軽にどうぞ。

ご購入後のサポートはDiscordにて行わせていただきます。（準備中）

[bcts (@bcts11) | Twitter](https://twitter.com/bcts11)
bcts = Blockchain Technology Supporter

**Stellar**

- XLM Deposit Address
    * GAHK7EEG2WWHVKDNT4CEQFZGKF2LGDSW2IVM4S5DP42RBW3K6BTODB4A
- XLM Deposit MEMO
    * 1051550088

**Ripple**

- XRP Deposit Tag
  - 102822377
- XRP Deposit Address
  - rEb8TK3gBgk5auZkwc6sHnwrGVJH8DuaLh

